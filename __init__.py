#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .cost_manage import CostDistribution, DistributionRule
from .production import Production


def register():
    Pool.register(
        Production,
        CostDistribution,
        DistributionRule,
        module='production_cost_manage', type_='model')