# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.modules.cost_manage import ComposeCostSource, CostCenter

__all__ = ['Production']

__metaclass__ = PoolMeta


class Production(ComposeCostSource, CostCenter):

    __name__ = 'production'

    @fields.depends('effective_date')
    def on_change_with_cost_date(self, field=None):
        return getattr(self, 'effective_date', None)

    def get_son_sources(self):
        return self.inputs

    @fields.depends('company')
    def on_change_with_currency_digits(self, name=None):
        if self.company:
            return self.company.currency.digits
        return 2

    @classmethod
    def run(cls, productions):
        super(Production, cls).run(productions)
        cls.cost_compute(productions)

    @classmethod
    def done(cls, productions):
        super(Production, cls).done(productions)
        cls.cost_compute(productions)

    def on_change_with_factor(self, field=None):
        return None